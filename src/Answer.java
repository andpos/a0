import java.text.SimpleDateFormat;
import java.util.*;

public class Answer {

   public static void main (String[] param) {
      // conversion double -> String
      double doubleValue = Math.PI;
      String stringValue = String.valueOf(doubleValue);
      System.out.println(stringValue);

      // conversion String -> int
      stringValue = "4096";
      int intValue = Integer.parseInt(stringValue);
      System.out.println(intValue);

      // "hh:mm:ss"
      stringValue = new SimpleDateFormat("HH:mm:ss").format(new Date());
      System.out.println(stringValue);

      // cos 45 deg
      doubleValue = Math.cos(Math.toRadians(45.0));
      System.out.println(doubleValue);

      // table of square roots
      System.out.println("  n | sqrt(n)\n-------------");
      for (int n = 0; n <= 100; n += 5) {
         System.out.println(String.format("%3d | %5.2f", n, Math.sqrt(n)));
      }

      String firstString = "ABcd12";
      String result = reverseCase (firstString);
      System.out.println ("\"" + firstString + "\" -> \"" + result + "\"");

      // reverse string

      String s = "How  many	 words   here";
      int nw = countWords (s);
      System.out.println (s + "\t" + String.valueOf (nw));

      // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!

      final int LSIZE = 100;
      ArrayList<Integer> randList = new ArrayList<Integer> (LSIZE);
      Random generaator = new Random();
      for (int i = 0; i < LSIZE; i++) {
         randList.add (Integer.valueOf (generaator.nextInt(1000)));
      }

      // minimal element
      Random rand = new Random();
      List<Integer> randomList = new ArrayList<Integer>();
      for (int n = 0; n < 100; ++n) {
         randomList.add(rand.nextInt(1000));
      }
      System.out.println("Random list: " + randomList.toString());
      System.out.println("Minimum: " + minimum(randomList));

      // HashMap tasks:
      //    create
      Map<String, String> map = new HashMap<>();
      map.put("ICD0001", "Algoritmid ja andmestruktuurid");
      map.put("ICD0009", "Hajussüsteemide ehitamine");
      map.put("ICD0010", "Tarkvaratehnika");
      map.put("ICD0012", "Tarkvara testimise alused ");
      map.put("ICD0015", "ASP.NET Veebirakendused");
      //    print all keys
      System.out.println("All keys: " + map.keySet().toString());
      //    remove a key
      map.remove("ICD0010");
      //    print all pairs
      System.out.println("All pairs: " + map.entrySet().toString());

      System.out.println ("Before reverse:  " + randList);
      reverseList(randList);
      System.out.println ("After reverse: " + randList);
      System.out.println ("Maximum: " + maximum(randList));
   }

   /** Finding the maximal element.
    * @param collection Collection of Comparable elements
    * @return maximal element.
    * @throws NoSuchElementException if <code> a </code> is empty.
    */
   public static <T extends Object & Comparable<? super T>> T maximum(Collection<? extends T> collection)
           throws NoSuchElementException {
      return Collections.max(collection);
   }

   /** Counting the number of words. Any number of any kind of
    * whitespace symbols between words is allowed.
    * @param text text
    * @return number of words in the text
    */
   public static int countWords (String text) {
      return new StringTokenizer(text).countTokens();
   }

   /** Case-reverse. Upper -> lower AND lower -> upper.
    * @param text string
    * @return processed string
    */
   public static String reverseCase (String text) {
      StringBuffer result = new StringBuffer();
      for (char character : text.toCharArray()) {
         if (Character.isLowerCase(character)) {
            character = Character.toUpperCase(character);
         } else if (Character.isUpperCase(character)) {
            character = Character.toLowerCase(character);
         }
         result.append(character);
      }
      return result.toString();
   }

   /** List reverse. Do not create a new list.
    * @param list list to reverse
    */
   public static <T extends Object> void reverseList (List<T> list)
      throws UnsupportedOperationException {
      Collections.reverse(list);
   }

   public static <T extends Object & Comparable<? super T>> T minimum(Collection<? extends T> collection)
           throws NoSuchElementException {
      return Collections.min(collection);
   }

}
